<?php

/**
 * Detect is PHP is used in CLI
 * 
 * @return boolean Determines if PHP is used in CLI.
*/
function isCLI() {
  return php_sapi_name() === 'cli';
}

/**
 * Client code to choose what input interpreter to use.
 */
function startInputInterpret($interpreter_type = 'CLI') {
  $interpreter = null;
  switch($interpreter_type) {
    case 'CLI':
      $interpreter = new CLIInterpreterCreator();
      break;
    case 'WEB':
      $interpreter = new WebInterpreterCreator();
      break;
    default:
      exit('Invalid input interpreter.');
      break;
  }

  return $interpreter->start();
}

/**
 * Detect if $index is the last in a zero-based array.
 * 
 * @param $index int The array element's index
 * @param $arr array Zero-based index array
 * @return boolean Determines if the $index is in last or not
*/
function isLastInArray($index = 0, $arr = []) {
  $arr_count = count($arr);
  return $index === ($arr_count - 1);
}