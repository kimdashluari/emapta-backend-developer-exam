<?php

// Helpers
require_once('helpers.php');

// Factory Creators
require_once('library/interpreter/InputInterpreterCreator.php');
require_once('library/interpreter/CLIInterpreterCreator.php');
require_once('library/interpreter/WebInterpreterCreator.php');

// Factory Products
require_once('library/interpreter/InputInterpreter.php');
require_once('library/interpreter/CLIInterpreter.php');
require_once('library/interpreter/WebInterpreter.php');

// Main game class
require_once('library/Game.php');

// Default Content Type
header('Content-type: text/plain');

// Add ASCII art
require_once('ascii-art.php');

// Input interpreter
$interpreter_type = isCLI() ? 'CLI' : 'WEB';
$initial_contestants = startInputInterpret($interpreter_type);

// Kickstart application
$game = new Game(
  $initial_contestants
);
$game->start();