<?php

/**
 * A simple number battles.
 * 
 * @author Kim Maravilla
*/

class Game
{
  /**
   * Stores array of initial contestants.
   * Used for the first call of the `playMatches` method.
   * 
   * @var array
  */
  protected $initial_contestants = [];

  /**
   * Stores count of initial contestants.
   * Used for the first call of the `playMatches` method.
   * 
   * @var integer
  */
  protected $initial_contestants_count = 0;

  /**
   * Stores count of initial rounds.
   * Used for the first call of the `playMatches` method.
   * 
   * @var integer
  */
  protected $initial_rounds_count = 0;

  /**
   * Counter to keep track of the current round.
   * 
   * @var integer
  */
  protected $current_round = 1;

  /**
   * * Counter to keep track of the current match.
   * 
   * @var integer
  */
  protected $current_match = 1;

  /**
   * Stores results of the game
   * 
   * @var integer
  */
  protected $results = [
    'matches' => [],
    'champion' => null,
    'total_bracket_matches' => 0,
    'total_rounds' => 0
  ];

  /**
   * Constructor.
   * 
   * @return void
   */
  public function __construct($initial_contestants = [])
  {
    /**
     * Updates initial contestants array.
     */
    $this->initial_contestants = $initial_contestants;
    
    /**
     * Count initial contestants
     */
    $this->initial_contestants_count = count($this->initial_contestants);
  
    /**
     * Computes initial rounds based on initial contestants count.
     */
    $this->initial_rounds_count = ceil($this->initial_contestants_count / 2);
  }

  /**
   * Main entry point of the game class.
   * Runs matches & rounds.
   * Displays game results.
   * 
   * @return void
   */
  public function start()
  {
    echo PHP_EOL . "Simulation: " . PHP_EOL . PHP_EOL;

    /**
     * Starts the first/initial match.
     * Match 1.
     */
    $this->playMatches(
      $this->initial_contestants,
      $this->initial_rounds_count
    );

    $this->displayResults();
  }

  /**
   * Displays game results.
   * 
   * @return void
   */
  public function displayResults()
  {
    $matches = $this->results['matches'];

    /**
     * Loops through matches
     */
    foreach ($matches as $m_index => $result) {

      /**
       * Loops through rounds under a match
       */
      foreach ($result['rounds'] as $round) {
        $current_round = $round['current_round'];
        $contestant_1 = $round['contestant_1'];
        $contestant_2 = $round['contestant_2'];
        $winner = $round['winner'];

        if ($round['winner_type'] === 'default_winner') {
          $match_word = 'Default';
        } else {
          $match_word = "{$contestant_1} vs {$contestant_2}";
        }

        echo "Round {$current_round}: {$match_word} Winner is: {$winner}" . PHP_EOL;
      }

      /**
       * Since `current_match` starts at 1.
       * We need to deduct 1 from `$m_index` to use it
       * to a zero-based index array correctly.
      */
      if (!isLastInArray($m_index - 1, $matches)) {
        echo PHP_EOL . "Next..." . PHP_EOL . PHP_EOL;
      } else {
        echo PHP_EOL;
      }
    }

    /**
     * Display other results.
     */
    echo "Champion: {$this->getChampion()} " . PHP_EOL;
    echo "Total Bracket Matches: {$this->results['total_bracket_matches']} " . PHP_EOL;
    echo "Total Rounds: {$this->results['total_rounds']}";
  }

  /**
   * A recursive function that determines the ff:
   * - Matches
   * - Rounds per matches
   * - Overall Winner/Champion
   * 
   * @param $contestants array Set of integers
   * @param $rounds_count integer Current rounds determined by
   *        the number of contestants
   * @return void
  */
  public function playMatches($contestants = [], $rounds_count = 0)
  {
    /**
     * Counts current contestants.
     */
    $contestants_count = count($contestants);

    /**
     * New set of contestants to track for the next match.
     */
    $next_contestants = [];
  
    /**
     * Determines if there's valid count of contestants.
     * 0 means no contestants and no need to further play matches.
     */
    if ($contestants_count === 0) return;
  
    /**
     * Detects overall winner/champion and stops recursion.
    */
    if ($contestants_count === 1) {

      $winner = $contestants[0];

      /**
       * Sets the overall champion.
       */
      $this->results['champion'] = $winner;
  
      /**
       * Detects default winner/champion if `initial_contestants_count` is equal to 1
       * and prevents recursion from running.
       */
      if ($this->initial_contestants_count === 1) {

        $this->results['matches'][$this->current_match]['rounds'][] = [
          'winner_type' => 'default_winner',
          'current_round' => $this->current_round,
          'contestant_1' => null,
          'contestant_2' => null,
          'winner' => $winner,
        ];

        $this->current_round++;
        $this->current_match++;

        $this->results['total_rounds']++;
        $this->results['total_bracket_matches']++;
      }
      
      return;
    }
  
    /**
     * Play match rounds based on current number of rounds.
     * Current Rounds = No. of contestants/2 then round of to the highest.
     */
    for ($i = 0; $i < $rounds_count; $i++) {
      /**
       * Get the contestant of `$contestant_2` starting from the
       * first index of the array.
       */
      $contestant_1 = $contestants[$i];

      /**
       * Get the contestant of `$contestant_1` starting from the last
       * index of the array.
       */
      $contestant_2 = $contestants[
        $contestants_count-($i + 1)
      ];
    
      /**
       * Determine the winner by comparing 2 contestants.
       * The larger one is the winner.
       */
      $winner = $contestant_1 > $contestant_2 ? $contestant_1 : $contestant_2;
  
      /**
       * Stores round winner used for recursive function call for `playMatches`.
       */
      $next_contestants[] = $winner;
  
      /**
       * Sets the `$winner_type` either `default_winner` or `match_winner`
       * to be used in the results display. See below the samples.
       * 
       * `default_winner` -> `Round 3: Default Winner is: 8`
       * `match_winner` -> `Round 4: 50 vs 8 Winner is: 50`
       */
      $winner_type = ($contestant_1 === $contestant_2) ? 'default_winner' : 'match_winner';

      $this->results['matches'][$this->current_match]['rounds'][] = [
        'winner_type' => $winner_type,
        'current_round' => $this->current_round,
        'contestant_1' => $contestant_1,
        'contestant_2' => $contestant_2,
        'winner' => $winner,
      ];

      $this->current_round++;
      $this->results['total_rounds']++;
    }

    $this->current_match++;
    $this->results['total_bracket_matches']++;

    /**
     * Computes the next rounds count based the next contestants
     * to be used in recursive function call for `playMatches`.
     */
    $next_rounds_count = ceil(
      count($next_contestants) / 2
    );

    /**
     * Recursive function call.
     * Starts a new match
    */
    $this->playMatches(
      $next_contestants,
      $next_rounds_count
    );
  }

  /**
   * Determines what to display in the results depending
   * if there's a winner or none.
   * 
   * @return string String representation of game winner..
  */
  public function getChampion()
  {
    $champion = $this->results['champion'];
    return is_null($champion) ? 'No champion' : $champion;
  }

  /**
   * Exposes `$this->results` for other purposes
   * 
   * @return $this->results array
   */
  public function getResults()
  {
    return $this->results;
  }
}