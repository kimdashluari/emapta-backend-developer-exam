<?php

/**
 * This Concrete Product implements the CLI interpreter.
 */
class CLIInterpreter implements InputInterpreter
{
  /**
   * Handles getting of max contestants number and the number
   * for each contestants.
   * Validates input to make sure all contestants are valid integers.
   * 
   * @return $initial_contestants array Set of numbers input from CLI
   */
  public function interpretInput(): array
  {
    $initial_contestants = [];
    $max_contestants = null;

    $stdin = fopen("php://stdin","rb");

    while (!is_numeric($max_contestants)) {
      echo "Enter the Number of Contestants: ";
      $max_contestants = trim(fgets($stdin));

      if (!is_numeric($max_contestants)) {
        echo "ERROR: Please enter a numeric value." . PHP_EOL;
      }

      if ((int) $max_contestants < 0) {
        echo "ERROR: Please enter a positive integer." . PHP_EOL;
        $max_contestants = null;
      }

      $c = 1;

      while ($c <= $max_contestants) {
        echo "Contestant # {$c}: ";
        $contestant_num = trim(fgets($stdin));

        if (is_numeric($contestant_num  )) {
          $initial_contestants[] = $contestant_num;
          $c++;
        } else {
          echo "ERROR: Please enter a numeric value." . PHP_EOL;
        }
      }
    }

    return $initial_contestants;
  }
}