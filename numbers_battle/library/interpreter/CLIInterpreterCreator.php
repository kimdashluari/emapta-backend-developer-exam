<?php

/**
 * This Concrete Creator supports CLI. Remember that this class also
 * inherits the 'start' method from the parent class. Concrete Creators are the
 * classes that the client actually uses.
 */
class CLIInterpreterCreator extends InputInterpreterCreator
{
  public function getInterpreter(): InputInterpreter
  {
    return new CLIInterpreter();
  }
}