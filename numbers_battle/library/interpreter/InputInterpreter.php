<?php

/**
 * The Product interface declares behaviors of various types of products.
 */
interface InputInterpreter
{
  public function interpretInput(): array;
}