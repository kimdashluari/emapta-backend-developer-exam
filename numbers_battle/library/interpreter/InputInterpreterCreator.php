<?php

/**
 * The Declares a factory method that can be used as a substitution for
 * the direct constructor calls of products.
 * 
 * This allows changing the type of the product being created by
 * InputInterpreter's subclasses.
 */
abstract class InputInterpreterCreator
{
  /**
   * The actual factory method. Note that it returns the abstract interpreter.
   * This lets subclasses return any concrete connectors without breaking the
   * superclass' contract.
   */
  abstract public function getInterpreter(): InputInterpreter;

  /**
   * When the factory method is used inside the Creator's business logic, the
   * subclasses may alter the logic indirectly by returning different types of
   * the interpreter from the factory method.
   */
  public function start()
  {
    $interpreter = $this->getInterpreter();
    return $interpreter->interpretInput();
  }
}