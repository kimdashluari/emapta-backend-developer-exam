<?php

/**
 * This Concrete Product implements the Web interpreter.
 */
class WebInterpreter implements InputInterpreter
{
  /**
   * @NOTE: Actual implementation is under construction.
   * @NOTE: Returns for now a static set of array.
   * 
   * @return array
   */
  public function interpretInput(): array
  {
    return [
      50,
      30,
      8,
      11,
      20
    ];
  }
}