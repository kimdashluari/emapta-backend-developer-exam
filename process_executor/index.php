<?php

interface DataProcess
{
  public function displayOutput($payload = []);
}

class DefaultExtractorData implements DataProcess
{
  public function displayOutput($payload = [])
  {
    $request_data = json_encode($payload['request_data'], JSON_PRETTY_PRINT);
    echo "Data Extracted: {$request_data}" . PHP_EOL;
    echo "Process Code: {$payload['process_code']}" . PHP_EOL;
  }
}

class ExamDataValidatorClass implements DataProcess
{
  public function displayOutput($payload = [])
  {

    $candidateHasTakenTheWrittenExam = $payload['taken_written_exam'] ? 'I\'ve taken the Exam' : 'I will take the exam later';
    $candidateHasTakenInterview = $payload['taken_interview'] ? 'I\'ve taken the Interview' : 'I will take the interview later';
    $score = $payload['score'] >= 75 ? 'Passed' : 'Failed';

    echo "Data Validation:" . PHP_EOL;
    echo "\tcandidateHasTakenTheWrittenExam: {$candidateHasTakenTheWrittenExam}" . PHP_EOL;
    echo "\tcandidateHasTakenInterview: {$candidateHasTakenInterview}" . PHP_EOL;
    echo "\tscore: {$score}" . PHP_EOL;
  }
}

class ExamAdditionalProcessClass implements DataProcess
{
  public function displayOutput($payload = [])
  {
    $candidateHasTakenTheWrittenExam = $payload['taken_written_exam'] ? 'Already taken the Exam' : 'Unable to take the Exam';
    $candidateHasTakenInterview = $payload['taken_written_exam'] ? 'Done with the Interview' : 'Unable to attend the Interview';

    echo "Additional Process:" . PHP_EOL;
    echo "\tcandidateHasTakenTheWrittenExam: {$candidateHasTakenTheWrittenExam}" . PHP_EOL;
    echo "\tcandidateHasTakenInterview: {$candidateHasTakenInterview}" . PHP_EOL;
  }
}

class ExamProcessManager
{
  protected $payload = [];

  public function __construct($payload = [])
  {
    $this->payload = $payload;
  }

  public function extractData(DataProcess $processor)
  {
    $processor->displayOutput($this->payload);
  }

  public function validateData(DataProcess $processor)
  {
    $processor->displayOutput($this->payload['request_data']);
  }

  public function additionalProcess(DataProcess $processor)
  {
    $processor->displayOutput($this->payload['request_data']);
  }
}

// Default Content Type
header('Content-type: text/plain');

// Add ASCII art
require_once('ascii-art.php');

$payload = [
  'process_code' => 'examProcessCode',
  'request_data' => [
    'taken_written_exam' => true,
    'taken_interview' => false,
    'score' => 90
  ],
];

$epm = new ExamProcessManager($payload);
$epm->extractData(
  new DefaultExtractorData
);

echo PHP_EOL;

$epm->validateData(
  new ExamDataValidatorClass
);

echo PHP_EOL;

$epm->additionalProcess(
  new ExamAdditionalProcessClass
);